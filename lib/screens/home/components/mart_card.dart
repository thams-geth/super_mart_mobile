import 'package:flutter/material.dart';
import 'package:super_mart_mobile/data/MartData.dart';
import 'package:super_mart_mobile/routes.dart';

class MartCard extends StatelessWidget {
  final MartData martData;
  const MartCard({
    Key? key,
    required this.martData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, RouteNames.detailsScreen),
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset(
              "assets/images/store_img.jpg",
              width: 100,
              height: 100,
              fit: BoxFit.cover,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(martData.name,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  SizedBox(width: 10),
                  Text(martData.featuredProducts),
                  SizedBox(width: 10),
                  Text(martData.location),
                  SizedBox(width: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Rating ${martData.rating}"),
                      Spacer(),
                      Text("Distance ${martData.distance}"),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
