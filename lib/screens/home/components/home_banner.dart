import 'package:flutter/material.dart';

import '../../../constants.dart';

class HomeBanner extends StatelessWidget {
  const HomeBanner({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 100,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
          color: Colors.amberAccent,
          child: Padding(
            padding: const EdgeInsets.all(defaultPadding),
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: "Offer One\n\n",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 5),
                  ),
                  TextSpan(text: "This is imited time offer Grab one quickly")
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
