import 'package:flutter/material.dart';
import 'package:super_mart_mobile/constants.dart';
import 'package:super_mart_mobile/data/MartData.dart';

import 'home_banner.dart';
import 'mart_card.dart';

class HomeBody extends StatelessWidget {
  const HomeBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Padding(
          padding: const EdgeInsets.all(defaultPadding),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              HomeBanner(),
              Text("Available Stores"),
              HomeList(),
            ],
          ),
        ),
      ),
    );
  }
}

class HomeList extends StatelessWidget {
  const HomeList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: martList.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: MartCard(
            martData: martList[index],
          ),
        );
      },
    );
  }
}
