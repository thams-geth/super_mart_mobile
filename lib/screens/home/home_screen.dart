import 'package:flutter/material.dart';
import 'package:super_mart_mobile/screens/home/components/home_body.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Super Mart")),
      body: HomeBody(),
    );
  }
}
