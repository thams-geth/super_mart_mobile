import 'package:flutter/material.dart';
import 'package:super_mart_mobile/constants.dart';
import 'package:super_mart_mobile/data/CategoryData.dart';
import 'package:super_mart_mobile/data/ProductsData.dart';

class DetailsBody extends StatelessWidget {
  const DetailsBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            Text(
              "Vignesh Super Mart",
              style: headingStyle,
            ),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: categoryList.length,
              itemBuilder: (context, index) => CategoryTitle(
                categoryData: categoryList[index],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CategoryTitle extends StatelessWidget {
  final CategoryData categoryData;
  const CategoryTitle({
    Key? key,
    required this.categoryData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          categoryData.categoryName,
          style: TextStyle(
              color: Colors.green, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: categoryData.productList.length,
          itemBuilder: (context, index) => ProductCard(
            productsData: categoryData.productList[index],
          ),
        )
      ],
    );
  }
}

class ProductCard extends StatelessWidget {
  final ProductsData productsData;
  const ProductCard({
    Key? key,
    required this.productsData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        children: [
          Image.asset(
            productsData.productImage,
            width: 80,
            height: 80,
          ),
          Expanded(
            flex: 20000,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  productsData.productName,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
                Text("Rs: ${productsData.price}"),
                Text(
                  productsData.productDescription,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                Text("Weight ${productsData.productWeight} ")
              ],
            ),
          ),
          Spacer(),
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 30,
                  height: 30,
                  child: Center(
                    child: Text(
                      "-",
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                    ),
                  ),
                ),
                Container(
                  width: 30,
                  height: 30,
                  child: Center(
                    child: Text(
                      "1",
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.green,
                  ),
                ),
                Container(
                  width: 30,
                  height: 30,
                  child: Center(
                    child: Text(
                      "+",
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
