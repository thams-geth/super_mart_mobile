import 'package:flutter/material.dart';
import 'package:super_mart_mobile/screens/details/components/details_body.dart';

class DetailsScreen extends StatelessWidget {
  const DetailsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Details")),
      body: DetailsBody(),
    );
  }
}
