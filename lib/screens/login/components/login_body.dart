import 'package:flutter/material.dart';
import 'package:super_mart_mobile/components/default_button.dart';
import 'package:super_mart_mobile/constants.dart';
import 'package:super_mart_mobile/routes.dart';

class LoginBody extends StatefulWidget {
  const LoginBody({Key? key}) : super(key: key);

  @override
  _LoginBodyState createState() => _LoginBodyState();
}

class _LoginBodyState extends State<LoginBody> {
  final _textController = TextEditingController();
  bool _isError = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(defaultPadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 40),
            Text("Enter your phoneno"),
            SizedBox(height: 20),
            TextField(
              controller: _textController,
              keyboardType: TextInputType.phone,
              maxLength: 10,
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.phone),
                hintText: "Enter Your Phone No",
                counterText: "",
                labelText: "Phone No",
                errorText: _isError ? "Please enter valid phone no" : null,
                border: outlineInputBorder(),
                focusedBorder: outlineInputBorder(),
                enabledBorder: outlineInputBorder(),
              ),
            ),
            SizedBox(height: 20),
            DefaultButton(
                name: "Login",
                press: () {
                  setState(() {
                    _textController.text.isEmpty ||
                            _textController.text.length < 10
                        ? _isError = true
                        : _isError = false;
                  });
                  if (!_isError)
                    Navigator.popAndPushNamed(context, RouteNames.homeScreen);
                })
          ],
        ),
      ),
    );
  }
}
