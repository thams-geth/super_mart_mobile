import 'package:flutter/material.dart';
import 'package:super_mart_mobile/screens/details/details_screen.dart';
import 'package:super_mart_mobile/screens/home/home_screen.dart';
import 'package:super_mart_mobile/screens/login/login_screen.dart';
import 'package:super_mart_mobile/screens/profile/profile_screen.dart';
import 'package:super_mart_mobile/screens/splash/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  RouteNames.splashScreen: (context) => SplashScreen(),
  RouteNames.loginScreen: (context) => LoginScreen(),
  RouteNames.homeScreen: (context) => HomeScreen(),
  RouteNames.detailsScreen: (context) => DetailsScreen(),
  RouteNames.profileScreen: (context) => ProfileScreen(),
};

class RouteNames {
  static final String splashScreen = "/splash";
  static final String onBoardingScreen = "/onboard";
  static final String loginScreen = "/login";
  static final String homeScreen = "/home";
  static final String detailsScreen = "/details";
  static final String profileScreen = "/profile";
}
