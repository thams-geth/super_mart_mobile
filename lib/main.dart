import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:super_mart_mobile/routes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Super Mart',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 0,
          textTheme: TextTheme(
            headline6: TextStyle(color: Colors.black),
          ),
        ),
        scaffoldBackgroundColor: Colors.white,
        textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme),
        // .apply(bodyColor: Colors.black),
        primarySwatch: Colors.blue,
      ),
      initialRoute: RouteNames.homeScreen,
      routes: routes,
    );
  }
}
