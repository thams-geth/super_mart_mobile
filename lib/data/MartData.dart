class MartData {
  final String name, featuredProducts, location, distance, image;
  final double rating;
  MartData(
      {required this.name,
      required this.featuredProducts,
      required this.location,
      required this.distance,
      required this.image,
      required this.rating});
}

List martList = [
  MartData(
      name: "Thams Super Mart",
      featuredProducts: "Onion,Tomoto",
      location: "Erode",
      distance: "2 km",
      image: "",
      rating: 4.4),
  MartData(
      name: "Vignesh Mart",
      featuredProducts: "Biscuits, Apple, Orange",
      location: "Chennai",
      distance: "1 km",
      image: "",
      rating: 3.7),
  MartData(
      name: "Lokesh Store",
      featuredProducts: "Nuts, badam, almonds",
      location: "Salem",
      distance: "0.3 km",
      image: "",
      rating: 2.1),
  MartData(
      name: "Jasmin MultiStore",
      featuredProducts: "Eggs,,Lays,Chips",
      location: "Chml",
      distance: "3 km",
      image: "",
      rating: 5),
  MartData(
      name: "Aravind Department",
      featuredProducts: "Some another featured products",
      location: "Banglore",
      distance: "3 km",
      image: "",
      rating: 3.8),
  MartData(
      name: "Sandeep Market",
      featuredProducts: "Nuts, badam, almonds",
      location: "Erode",
      distance: "3 km",
      image: "",
      rating: 4),
  MartData(
      name: "Girish Store",
      featuredProducts: "Nuts, badam, almonds",
      location: "Chennai",
      distance: "3 km",
      image: "",
      rating: 4),
  MartData(
      name: "Sri fruit store",
      featuredProducts: "Nuts, badam, almonds",
      location: "Chennai",
      distance: "3 km",
      image: "",
      rating: 4),
  MartData(
      name: "Sri fruit store",
      featuredProducts: "Biscuits, Apple, Orange",
      location: "Chennai",
      distance: "3 km",
      image: "",
      rating: 4),
  MartData(
      name: "Sri fruit store",
      featuredProducts: "Onion,Tomoto",
      location: "Chennai",
      distance: "3 km",
      image: "",
      rating: 5),
  MartData(
      name: "Sri fruit store",
      featuredProducts: "Biscuits, Apple, Orange",
      location: "Chennai",
      distance: "3 km",
      image: "",
      rating: 4),
];
