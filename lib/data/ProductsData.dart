class ProductsData {
  final String productName, productDescription, productImage;
  final double price, productWeight;
  final int itemCount, maxItemCanAdd;

  ProductsData(
      {required this.price,
      required this.productWeight,
      required this.itemCount,
      required this.maxItemCanAdd,
      required this.productImage,
      required this.productName,
      required this.productDescription});
}

List productList = [
  ProductsData(
      price: 20,
      productWeight: 1.200,
      itemCount: 1,
      maxItemCanAdd: 3,
      productImage: "assets/images/potato.jpeg",
      productName: "Tomato",
      productDescription:
          "This is banglore tomoto. Clean and red. Big is size"),
  ProductsData(
      price: 5,
      productWeight: 0.50,
      itemCount: 1,
      maxItemCanAdd: 10,
      productImage: "assets/images/lays.jpeg",
      productName: "Lays Green",
      productDescription: "This is bgreen Lays, cruncy and salt flavour"),
  ProductsData(
      price: 50,
      productWeight: 1.00,
      itemCount: 1,
      maxItemCanAdd: 2,
      productImage: "assets/images/onion.jpeg",
      productName: "Onion",
      productDescription: "Small Onion for cooking"),
  ProductsData(
      price: 10,
      productWeight: 0.300,
      itemCount: 1,
      maxItemCanAdd: 3,
      productImage: "assets/images/Cabbage.jpeg",
      productName: "Cabbage",
      productDescription: "While cabbage with leaf"),
  ProductsData(
      price: 20,
      productWeight: 1.200,
      itemCount: 1,
      maxItemCanAdd: 3,
      productImage: "assets/images/potato.jpeg",
      productName: "Tomato",
      productDescription:
          "This is banglore tomoto. Clean and red. Big is size"),
];
